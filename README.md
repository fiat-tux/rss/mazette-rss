# Générateur de flux RSS pour les actus de Mazette !

Le flux RSS reprend uniquement les [actus du mois courant](https://mazette.media/actu).

ATTENTION ! Ce script n’a plus besoin d’un compte pour fonctionner, [Mazette]((https://mazette.media) permettant de voir les dessins d’actualité sans abonnement.

## Dépendances

```
apt install libmojolicious-perl libxml-rss-perl
```

## Configuration

La configuration est gérée par trois variables d’environnement :
- `MAZETTE_RSSFILE` : chemin du fichier RSS à créer

## Installation

```
wget https://framagit.org/fiat-tux/rss/mazette-rss/-/raw/master/mazette-rss.pl -O /opt/mazette-rss.pl
chmod +x /opt/mazette-rss.pl
```

## Utilisation

```
MAZETTE_RSSFILE="/var/www/mon_flux_rss_des_actus_mazette.rss" /opt/mazette-rss.pl
```

## Cron

Voici un exemple de tâche cron pour mettre à jour le flux toutes les 6 heures :
```
45 */6 * * * MAZETTE_RSSFILE="/var/www/mon_flux_rss_des_actus_mazette.rss" /opt/mazette-rss.pl
```

## Licence

Affero GPLv3. Voir le fichier [LICENSE](LICENSE).
